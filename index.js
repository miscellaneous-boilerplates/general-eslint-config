const fs = require("fs");
const path = require("path");

// eslint-disable-next-line security/detect-non-literal-fs-filename
module.exports = JSON.parse(fs.readFileSync(path.join(__dirname, ".eslintrc-config.json")));