# General configuration for ESLint

## Usage
1. Install via `npm install git+https://gitlab.com/miscellaneous-boilerplates/general-eslint-config.git`.
2. Make sure to extend `"eslint-config-general"` in the `.eslintrc.json` config file or the `eslintConfig` field in `package.json`:
```json
"extends": [
  "eslint-config-general"
]
```
3. Run the desired `eslint` command like `eslint "**/*.js"` or use the script `lint.sh` with either `./lint.sh` or `bash lint.sh`.

## Development
### Linting
In order to lint the project files, after running `npm install`, one can either run `eslint "**/*.js"` or the script `lint.sh` for convenience, as detailed in [Usage](#usage).

The project uses ESLint itself to lint the project files.  

When running `eslint` without a configuration file specified, ESLint uses the default configuration file, `.eslintrc.json`. This file extends the `"eslint-config-general"`, the most recent committed and pushed upstream version of the configuration in this project, installed by `npm install`.  
Thus, the ESLint configuration for this project is the latest published version of the configuration specified in the project itself.  

### Testing
The package simply exports the configuration found in the `.eslintrc-config.json` file.  
To test the local version of the config, use the `-c` option for `eslint` with the config file, `.eslintrc-config.json`:
```shell
eslint -c ".eslintrc-config.json" "**/*.js"
```
Alternatively, for convenience, run the script `lint-local-config.sh` with either `./lint-local-config.sh` or `bash lint-local-config.sh`.

*TODO Should decide whether to publish the package on the NPM registry or not.*